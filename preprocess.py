from os.path import join, dirname

import pandas as pd

data_file = join(dirname(__file__), "data", "data.csv")
data = pd.read_csv(data_file)

data = data[['BANDWIDTH_TOTAL', 'MAX_USER']]
data.to_csv("data/train.csv", index=True, columns=None)
print(0)
